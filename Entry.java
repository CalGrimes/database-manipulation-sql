import java.sql.*;
import java.util.Scanner;

public class Entry {
    private static final Scanner S = new Scanner(System.in);

    private static Connection c = null;
    private static ResultSet rs = null;

    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            c = DriverManager.getConnection("jdbc:derby://localhost:1527/5102comp", "cmpcgrim", "cmpcgrim"); // ToDo : Specify Connection String !
            Statement s = c.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = s.executeQuery("SELECT S.`num`, M.`name`, L.`due` "
            + "FROM student S INNER JOIN loan L "
            + "ON S.`num` = L.`num` "
            + "WHERE (YEAR(L.due) = YEAR(CURDATE())) "
            + "ORDER BY L.`due` ASC"); // ToDo : Specify SELECT Statement !

            String choice = "";

            do {
                System.out.println("-- MAIN MENU --");
                System.out.println("1 - Browse ResultSet");
                System.out.println("2 - Invoke Procedure");
                System.out.println("Q - Quit");
                System.out.print("Pick : ");

                choice = S.next().toUpperCase();

                switch (choice) {
                    case "1" : {
                        browseResultSet();
                        break;
                    }
                    case "2" : {
                        invokeProcedure();
                        break;
                    }
                }
            } while (!choice.equals("Q"));

            c.close();

            System.out.println("Bye Bye :)");
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private static void browseResultSet() throws Exception {
        if (rs.first()) {
            System.out.println("-- RESULTS --");
            
            do {
                System.out.println(String.format("Student No : %s\tName : %s\tDue : %s", rs.getInt("no"), rs.getString("name"), rs.getDate("due")));
            } while(rs.next());
        } 
        else {
            System.out.println("-- NO DATA --");
        }
    }

    private static void invokeProcedure() throws Exception {
        System.out.println("-- INVOKE PROCEDURE --");
        // ToDo : Accept Book ISBN & Student No !
        System.out.println("Enter isbn: ");
        String studentISBN = S.next();
        
        System.out.println("Enter student no: ");
        int studentNo = Integer.parseInt(S.next());
        // ToDo : Declare, Configure & Invoke CallableStatement !
      
        CallableStatement cs = c.prepareCall("{CALL new_loan(?,?)}");
        cs.setString("new_loan", studentISBN);
        cs.setInt("new_loan", studentNo);
        cs.executeUpdate();
        
        System.out.println("Complete.");
        
    }
}
