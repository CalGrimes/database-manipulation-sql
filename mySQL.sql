-- reset DB
DROP TRIGGER IF EXISTS loan_update_overdue;
DROP TABLE IF EXISTS loan_audit;

DROP PROCEDURE IF EXISTS new_Loan; 

DROP VIEW IF EXISTS cmp_students;


DROP TABLE IF EXISTS loan;
DROP TABLE IF EXISTS copy;
DROP TABLE IF EXISTS book;
DROP TABLE IF EXISTS student;


-- create tables
CREATE TABLE book (isbn CHAR(17) NOT NULL, 
 title VARCHAR(30) NOT NULL,
 author VARCHAR(30) NOT NULL,
 CONSTRAINT pri_book PRIMARY KEY (isbn), 
 CONSTRAINT uni_title UNIQUE (title));
CREATE TABLE student (`no` INT NOT NULL,
 `name` VARCHAR(30) NOT NULL,
 school  CHAR(3) NOT NULL,
 embargo BIT(1) NOT NULL DEFAULT b'0', 
 CONSTRAINT pri_student PRIMARY KEY (`no`));
CREATE TABLE copy (
`code`  INT NOT NULL,
 isbn CHAR(17) NOT NULL,
 duration TINYINT NOT NULL,
 CONSTRAINT pri_copy PRIMARY KEY (`code`),
 CONSTRAINT for_copy FOREIGN KEY (isbn)
 REFERENCES book (isbn) ON UPDATE CASCADE ON DELETE RESTRICT,
 CONSTRAINT chk_duration CHECK (duration IN (7, 14, 21)));
CREATE TABLE loan (
`code` INT NOT NULL,
 `no` INT NOT NULL, 
 taken DATE NOT NULL,
 due DATE NOT NULL,
 `return` DATE NULL, 
 CONSTRAINT pri_loan PRIMARY KEY (`code`, `no`, taken),
 CONSTRAINT for1_loan FOREIGN KEY (`code`)
 REFERENCES copy (`code`) ON UPDATE CASCADE ON DELETE RESTRICT,
 CONSTRAINT for2_loan FOREIGN KEY (`no`) 
 REFERENCES student (`no`) ON UPDATE CASCADE ON DELETE RESTRICT
 );
 
 -- Create indexes
 CREATE INDEX index1_copy ON copy (isbn);
 CREATE INDEX index1_loan ON loan (`code`);
 CREATE INDEX index2_loan ON loan (`no`);

 -- Insert book data
 INSERT INTO book(isbn, title, author)
 VALUES("111-2-33-444444-5", "Pro JavaFX", "Dave Smith");
  INSERT INTO book(isbn, title, author)
 VALUES("222-3-44-555555-6", "Oracle Systems", "Kate Roberts");
 INSERT INTO book(isbn, title, author)
 VALUES("333-4-55-666666-7", "Expert jQuery", "Mike Smith");

-- Insert copy data
 INSERT INTO copy(`code`, isbn, duration)
VALUES (1011, "111-2-33-444444-5", 21);
 INSERT INTO copy(`code`, isbn, duration)
VALUES (1012, "111-2-33-444444-5", 14);
 INSERT INTO copy(`code`, isbn, duration)
VALUES (1013, "111-2-33-444444-5", 7);
 INSERT INTO copy(`code`, isbn, duration)
VALUES (2011, "222-3-44-555555-6", 21);
 INSERT INTO copy(`code`, isbn, duration)
VALUES (3011, "333-4-55-666666-7", 7);
 INSERT INTO copy(`code`, isbn, duration)
VALUES (3012, "333-4-55-666666-7", 14);

-- Insert student data
 INSERT INTO student(`no`, `name`, school, embargo)
VALUES(2001, "Mike", "CMP", 0);
 INSERT INTO student(`no`, `name`, school, embargo)
VALUES(2002, "Andy", "CMP", 1);
 INSERT INTO student(`no`, `name`, school, embargo)
VALUES(2003, "Sarah", "ENG", 0);
 INSERT INTO student(`no`, `name`, school, embargo)
VALUES(2004, "Karen", "ENG", 1);
 INSERT INTO student(`no`, `name`, school, embargo)
VALUES(2005, "Lucy", "BUE", 0);


-- Insert loan data
 INSERT INTO loan(`code`, `no`, taken, due, `return`)
VALUES (1011, 2002, "2020.01.10", "2020.01.31", "2020.01.31");
 INSERT INTO loan(`code`, `no`, taken, due, `return`)
VALUES (1011, 2002, "2020.02.05", "2020.02.26", "2020.02.23");
 INSERT INTO loan(`code`, `no`, taken, due, `return`)
VALUES (1011, 2003, "2020.05.10", "2020.05.31", NULL);
 INSERT INTO loan(`code`, `no`, taken, due, `return`)
VALUES (1013, 2003, "2019.03.02", "2019.03.16", "2019.03.10");
 INSERT INTO loan(`code`, `no`, taken, due, `return`)
VALUES (1013, 2002, "2019.08.02", "2019.08.16", "2019.08.16");
 INSERT INTO loan(`code`, `no`, taken, due, `return`)
VALUES (2011, 2004, "2018.02.01", "2018.02.22", "2018.02.20");
 INSERT INTO loan(`code`, `no`, taken, due, `return`)
VALUES (3011, 2002, "2020.07.03", "2020.07.10", NULL);
 INSERT INTO loan(`code`, `no`, taken, due, `return`)
VALUES (3011, 2005, "2019.10.10", "2019.10.17", "2019.10.20");

-- Create view
CREATE VIEW cmp_students
AS
	SELECT `no`, `name`, school, embargo
		FROM student
		WHERE school = 'CMP'
    WITH CHECK OPTION;
-- View test rejection

    
    
DELIMITER $$
	CREATE PROCEDURE new_Loan(IN book_isbn CHAR(17), IN student_no INT)
		BEGIN

		-- declaring variable to search for book copy
        -- declaring variable to test for loan record in loan table
		DECLARE copy_code, loan_test INT;
        
        -- declaring variable to test for a successfully loaned out book
        -- declaring variable to inform end of cursor
        DECLARE loan_added, complete BOOLEAN;
        
        -- due date and current date
        DECLARE due, cur_date DATE;
        
        -- declaring variable to store no of days for new loan
        DECLARE copy_dur TINYINT;
        
        -- declaring variable to check if student is allowed to loan a book
        DECLARE embargo_s BIT(1) DEFAULT b'1';
        
        -- declaring copy cursor based from `code` based on isbn
        DECLARE copy_c CURSOR FOR
			SELECT `code`
            FROM copy
            WHERE isbn = book_isbn;
            
		DECLARE CONTINUE HANDLER FOR NOT FOUND
			SET complete = TRUE;
            
		-- open cursor
        OPEN copy_c;
        
        -- get student embargo
        SET embargo_s = (
        SELECT embargo
		FROM student
		WHERE `no` = student_no);
        
        SELECT embargo_s;
        
        
		-- check if student's not under embargo
        --
		-- if not display message
		IF (embargo_s IS NULL OR embargo_s = b'1') THEN
				SIGNAL SQLSTATE '45000' 
                SET MESSAGE_TEXT =
                'The student is unable to loan a book';
		END IF;
        
        SET loan_added = FALSE;
        
        SET copy_code = 0;
        
        -- search through copies for an available book
        copy_loop : LOOP
			
		-- get the item from the list
        FETCH NEXT FROM copy_c INTO copy_code;
        
        -- if there are no more codes in list
        IF(complete) THEN 
			LEAVE copy_loop;
		END IF;
        
        -- if a copy is available-> loan test will be null
        --
        -- if return is null-> book is out on loan already
        SET loan_test = (
        SELECT `code` FROM loan
		WHERE (`code` = copy_code) AND (`return` IS NULL));

		-- if copy available-> loan_test will be null
        IF(loan_test IS NULL) THEN
        
			-- get current date for taken
			SET cur_date = CURRENT_DATE();
        
			-- get duration
			SET copy_dur = (
			SELECT duration FROM copy
			WHERE `code` = copy_code);
        
			-- calculate due date from duration and current date
			SET due = DATE_ADD(cur_date, INTERVAL copy_dur DAY);
        
			-- new loan to student->
			INSERT INTO loan(`code`, `no`, taken, due)
				VALUES (copy_code, student_no, cur_date, due);
        
			-- after insert set inserted to true
			SET loan_added = TRUE;
        
			LEAVE copy_loop;
        
		END IF;

		END LOOP;
		
        -- close copy cursor
        CLOSE copy_c;
        
        -- if theres no suitable book to loan inform user
        IF(loan_added = FALSE) THEN
        SIGNAL SQLSTATE '45000' 
                SET MESSAGE_TEXT =
                'No copies of requested book available';
		END IF;
		END$$
DELIMITER ;

CALL new_Loan("111-2-33-444444-5", 2001);


-- create audit trail table
 CREATE TABLE loan_audit (
 audit_no INT NOT NULL AUTO_INCREMENT,
 `no` INT NOT NULL,
 taken DATE NOT NULL,
 due DATE NOT NULL,
 `return` DATE NULL,
 updatedBy NVARCHAR(128),
 updatedOn DATE NOT NULL,
 CONSTRAINT pri_aduit PRIMARY KEY(`audit_no`)
 );

DELIMITER $$
	CREATE TRIGGER loan_update_overdue	
	 BEFORE UPDATE ON loan FOR EACH ROW
        BEGIN
			IF OLD.`return` IS NULL AND (CURDATE > OLD.due) THEN
			INSERT INTO loan_audit(audit_no, `no`, taken, due, `return`)
				VALUES(audit_no, `no`, NEW.taken, NEW.due, `return`, CURRENT_USER(), CURRENT_TIMESTAMP());
			END IF;
        END$$
DELIMITER ;
    
    
-- 1. Fetch every book’s isbn, title & author.
SELECT isbn, title, author FROM book;
--
-- 2. Fetch every student’s no, name & school in descending order by school
SELECT `no`, `name`, school
FROM student 
ORDER BY school DESC;
--
-- 3. Fetch any book’s isbn & title where that book’s author contains the string “Smith”.
SELECT isbn, title 
FROM book 
WHERE author LIKE '%Smith%';
--
-- 4. Calculate the latest due date for any book copy.
SELECT MAX(due) AS 'Latest'
FROM loan;
--
-- 5. Modify the query from Q4 to now fetch only the student no
SELECT `no` 
FROM loan
WHERE due = (
SELECT MAX(due) 
FROM loan);
--
-- 6. Modify the query from Q5 to also fetch the student name
SELECT `no`, `name`
FROM student 
WHERE `no` = (
		SELECT loan.`no` 
		FROM loan 
		WHERE loan.due =(
			SELECT MAX(due) 
			FROM loan));
--
-- 7. Fetch the student no, copy code & due date for loans in the current year which have not yet been returned
SELECT `no`, `code`, due 
FROM loan 
WHERE (YEAR(taken) = YEAR(CURDATE()))
	AND (`return` IS NULL);
--

-- 8. Uniquely fetch the student no & name along with the book isbn & title for students who have loaned a 7 day duration book.
SELECT DISTINCT S.`no`, S.`name`, B.isbn, B.title
	FROM copy C INNER JOIN loan L
		ON L.`code` = C.`code`
	INNER JOIN student S
		ON L.`no` = S.`no`
	INNER JOIN book B
		ON C.isbn = B.isbn
	WHERE C.duration = 7;
--
-- 9. Solve the problem from Q6 using JOINS where possible
    SELECT S.`no`, S.`name`
		FROM student S INNER JOIN  loan L 
			ON S.`no` = L.`no`
		WHERE L.due = (SELECT MAX(L.due) FROM loan);
--
-- 10. Calculate then display the loan frequency for every book
	SELECT B.isbn, COUNT(C.isbn) AS 'Count' FROM loan L INNER JOIN copy C
    ON L.`code` = C.`code`
    INNER JOIN book B
    ON C.isbn = B.isbn
    GROUP BY B.isbn;
--

-- 11. Modify the query from Q10 to only show a book when it has been loaned two or more times.
	SELECT B.isbn, COUNT(C.isbn) AS 'Count' FROM loan L INNER JOIN copy C
    ON L.`code` = C.`code`
    INNER JOIN book B
    ON C.isbn = B.isbn
    GROUP BY B.isbn
    HAVING COUNT > 1;
--
